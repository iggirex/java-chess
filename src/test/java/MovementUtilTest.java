import com.iggirex.java_chess.board.Board;
import com.iggirex.java_chess.board.Cell;
import com.iggirex.java_chess.utils.MovementUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MovementUtilTest {

    Board testBoard;

    @Before
    public void initialize() {
        testBoard = new Board();
    }

    @Test
    public void calculateSingleNewLocationTest() {
        int[] testCurrentPiecePosition = {3, 4};
        int[] testDirection = {1, 1};
        int[] newPieceLocation = MovementUtil.calculateNewPieceLocation(testCurrentPiecePosition, testDirection);
        int[] expectedPieceLocation = {4, 5};

        Assert.assertArrayEquals(newPieceLocation, expectedPieceLocation);
    }

    @Test
    public void calculateAllNewLocationTest() {

        for ( Cell cell : testBoard.getAllCells() ) {
            for ( int[] direction : MovementUtil.ALL_MOVEMENT_DIRECTIONS ) {
                int[] currentLocation = cell.getLocation();
                int[] expectedNewLocation = new int[2];
                expectedNewLocation[0] = currentLocation[0] + direction[0];
                expectedNewLocation[1] = currentLocation[1] + direction[1];

                int[] calculatedNewLocation = MovementUtil.calculateNewPieceLocation(currentLocation, direction);

                Assert.assertArrayEquals(expectedNewLocation, calculatedNewLocation);
            }
        }
    }

}
