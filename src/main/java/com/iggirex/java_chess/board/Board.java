package com.iggirex.java_chess.board;

import lombok.Data;

import java.util.ArrayList;

@Data
public class Board {
    private ArrayList<Cell> allCells = new ArrayList();

    public Board() {
        initializeBoard();
    }

    public void initializeBoard() {
        for (int i=1; i < 9; i++) {
            for (int j=1; j < 9; j++) {
                int[] thisLocation = {j, i};

                Cell cell = new Cell(thisLocation);
                allCells.add(cell);
            }
        }
    }

}
