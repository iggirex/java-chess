package com.iggirex.java_chess.board;

import com.iggirex.java_chess.pieces.Piece;
import lombok.Data;

@Data
public class Cell {

    int[] location;
    boolean empty = true;
    Piece occupyingPiece;

    Cell(int[] location) {
        this.location = location;
    }

}
