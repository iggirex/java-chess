package com.iggirex.java_chess.pieces;

import java.util.Arrays;
import java.util.List;

public class Pawn extends Piece {

    List<String> moves = Arrays.asList("0, 1", "1, 1", "-1, 1");

    public void move() {
        this.yLocation++;
        System.out.println("Pawn moving");
    }

}
