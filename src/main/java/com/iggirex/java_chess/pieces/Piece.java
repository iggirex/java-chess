package com.iggirex.java_chess.pieces;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class Piece {

    boolean inPlay;
    String type;
    int xLocation;
    int yLocation;
    String startingLocation;
    ArrayList<String> moves;
    String color;

    abstract void move();

}
