package com.iggirex.java_chess;


import com.iggirex.java_chess.pieces.Pawn;

public class Main {

    public static void main(String[] args) {

        Pawn pawn = new Pawn();
        pawn.move();

    }
}
