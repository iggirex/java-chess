package com.iggirex.java_chess.utils;

import com.iggirex.java_chess.board.Board;
import com.iggirex.java_chess.board.Cell;

public class MovementUtil {

    public static final int[][] ALL_MOVEMENT_DIRECTIONS = {{1,1}, {1, 0}, {0, 1},
            {1, -1}, {-1, 1}, {-1, -1}, {-1, 0}, {0, -1}};

    public static int[] calculateNewPieceLocation(int[] currentPieceLocation, int[] direction) {
        int[] newPieceLocation = new int[2];

        int xDirection = direction[0];
        int yDirection = direction[1];

        newPieceLocation[0] = currentPieceLocation[0] + xDirection;
        newPieceLocation[1] = currentPieceLocation[1] + yDirection;

        return newPieceLocation;
    }

    public static Cell getCellFromLocation(Board board, int[] location) {

        for (Cell cell : board.getAllCells()) {
            if (cell.getLocation() == location) {
                return cell;
            }
        }
        return null;
    }

}
